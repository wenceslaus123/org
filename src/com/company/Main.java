package com.company;

import com.company.model.Bank;
import com.company.model.BlackMarket;
import com.company.model.Exchanger;
import com.company.model.Organization;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("enter uah:");
        Scanner scanner = new Scanner(System.in);
        int uah = Integer.parseInt(scanner.nextLine());

        Bank bank = Generator.getBank();
        BlackMarket blackMarket = Generator.getBlackMarket();
        Exchanger exchanger = Generator.getExchanger();

        if (bank.exchange(uah) > 0f) {
            System.out.println(String.format("in %s: %.2f usd", bank.getName(), bank.exchange(uah)));
        }

        if (blackMarket.exchange(uah) > 0f) {
            System.out.println(String.format("in blackMarket: %.2f usd", blackMarket.exchange(uah)));
        }

        if (exchanger.exchange(uah) > 0f) {
            System.out.println(String.format("in exchanger: %.2f usd", exchanger.exchange(uah)));
        }

    }
}
