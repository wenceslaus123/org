package com.company;

import com.company.model.Bank;
import com.company.model.BlackMarket;
import com.company.model.Exchanger;

public class Generator {

    public static Bank getBank() {
        return new Bank("Oshad", 26.5f, 150_000);
    }

    public static BlackMarket getBlackMarket() {
        return new BlackMarket("BlackMarket", 26.3f);
    }

    public static Exchanger getExchanger() {
        return new Exchanger("Exchanger", 26.7f, 5_000);
    }
}
